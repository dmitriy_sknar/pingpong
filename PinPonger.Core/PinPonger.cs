﻿using System;
using System.Threading;
using RabbitMQ.Wrapper;

namespace PinPonger.Core {
    public class PinPonger : IPingPong, IDisposable {
        private RabbitClient RabbitClient;
        private readonly bool _isPinger;

        public PinPonger(bool isPinger) {
            _isPinger = isPinger;
        }

        public void Init() {
            RabbitClient = new RabbitClient(_isPinger);
            RabbitClient.Init();
            AttachEventHandlers();
        }

        public void Start() {
            //Pinger sends message first. Ponger must start to answer.
            if (_isPinger) SendFirstMessage();
            //do not close the app
            Thread.Sleep(Timeout.Infinite);
        }

        private void SendFirstMessage() {
            RabbitClient.SendMessageToQueue();
        }

        private void AttachEventHandlers() {
            RabbitClient.ReceiveMessage += OnMessageReceived;
            RabbitClient.SendMessage += OnMessageSent;
        }

        private void DetachEventHandlers() {
            RabbitClient.ReceiveMessage -= OnMessageReceived;
            RabbitClient.SendMessage -= OnMessageSent;
        }

        private void OnMessageReceived(object sender, string message) {
            Console.WriteLine($"{message} received at {DateTime.Now}");

            //synchronous so far. Make async in case logic becomes more complex 
            Thread.Sleep(2500);
            RabbitClient.SendMessageToQueue();
        }

        private void OnMessageSent(object sender, string message) {
            Console.WriteLine($"{message} sent at {DateTime.Now}");
        }

        public void Dispose() {
            DetachEventHandlers();
            RabbitClient?.Dispose();
        }
    }
}
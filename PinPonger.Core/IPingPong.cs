﻿namespace PinPonger.Core
{
    public interface IPingPong {
        void Init();

        void Start();
    }
}
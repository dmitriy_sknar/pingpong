﻿using System;
using System.Text;
using System.Threading;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace RabbitMQ.Wrapper {
    public class RabbitClient : IDisposable {
        public EventHandler<string> SendMessage;
        public EventHandler<string> ReceiveMessage;

        private const string USER = "guest";
        private const string PASS = "guest";
        private const string HOST = "localhost";
        private const string VHOST = "/";
        private readonly bool _isPingerInstance;

        private IConnection conn;
        private IModel sendChannel;
        private IModel receiveChannel;
        private EventingBasicConsumer consumer;

        // this consumer tag identifies the subscription
        // when it has to be cancelled
        private string consumerTag;

        public RabbitClient(bool isPingerInstance) {
            _isPingerInstance = isPingerInstance;
        }

        public void Init() {
            ConnectionFactory factory = new ConnectionFactory();
            factory.UserName = USER;
            factory.Password = PASS;
            factory.VirtualHost = VHOST;
            factory.HostName = HOST;
            factory.RequestedHeartbeat = new TimeSpan(0, 1, 0);

            conn = factory.CreateConnection();
            InitSend();
            InitReceive();
        }

        private void InitSend() {
            var queue = GetSendReceiveQueue(_isPingerInstance);
            var exchange = GetSendReceiveExchange(_isPingerInstance);
            var routingKey = GetSendReceiveRoutingKey(_isPingerInstance);

            Console.WriteLine($"Init send: queue: {queue}, exchange: {exchange}, routingKey: {routingKey}");
            sendChannel = InitChannel(exchange, queue, routingKey);
        }

        private void InitReceive() {
            var queue = GetSendReceiveQueue(!_isPingerInstance);
            var exchange = GetSendReceiveExchange(!_isPingerInstance);
            var routingKey = GetSendReceiveRoutingKey(!_isPingerInstance);

            Console.WriteLine($"Init receive: queue: {queue}, exchange {exchange}, routingKey: {routingKey}");
            bool isCunsumer = true;
            receiveChannel = InitChannel(exchange, queue, routingKey, isCunsumer);

            ListenQueue();
        }

        private IModel InitChannel(string exchange, string queue, string routingKey, bool isConsumer = false) {
            var channel = conn.CreateModel();

            channel.ExchangeDeclare(exchange, ExchangeType.Direct);
            if (isConsumer) {
                //autodelete is true to clean-up queues on last consumer has unsubscribed
                channel.QueueDeclare(queue, false, false, true, null);
                channel.QueueBind(queue, exchange, routingKey, null);
            }

            channel.BasicQos(0, 1, false);
            return channel;
        }

        public void SendMessageToQueue(string message) {
            byte[] messageBodyBytes = Encoding.UTF8.GetBytes(message);
            sendChannel.BasicPublish(GetSendReceiveExchange(_isPingerInstance),
                GetSendReceiveRoutingKey(_isPingerInstance), null, messageBodyBytes);
        }

        public void SendMessageToQueue() {
            var message = GetMsgToSend(_isPingerInstance);
            SendMessageToQueue(message);
            SendMessage?.Invoke(this, message);
        }

        public void ListenQueue() {
            consumer = new EventingBasicConsumer(receiveChannel);
            consumer.Received += OnMessageReceived;
            consumerTag = receiveChannel.BasicConsume(GetSendReceiveQueue(!_isPingerInstance), false, consumer);
        }

        private void OnMessageReceived(object ch, BasicDeliverEventArgs ea) {
            byte[] body = ea.Body.ToArray();
            string message = Encoding.UTF8.GetString(body);
            ReceiveMessage?.Invoke(this, message);

            receiveChannel.BasicAck(ea.DeliveryTag, false);
        }

        private string GetSendReceiveExchange(bool type) {
            return type ? "ping_exchange" : "pong_exchange";
        }

        private string GetSendReceiveQueue(bool type) {
            return type ? "pong_queue" : "ping_queue";
        }

        private string GetSendReceiveRoutingKey(bool type) {
            return type ? "pong_key" : "ping_key";
        }

        private string GetMsgToSend(bool type) {
            return type ? "ping" : "pong";
        }

        public void Dispose() {
            receiveChannel?.BasicCancel(consumerTag);
            sendChannel?.QueueDelete(GetSendReceiveQueue(_isPingerInstance), false, false);
            receiveChannel?.QueueDelete(GetSendReceiveQueue(!_isPingerInstance), false, false);
            sendChannel?.Close();
            receiveChannel?.Close();
            conn?.Close();
        }
    }
}
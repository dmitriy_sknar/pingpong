﻿using System;
using PinPonger.Core;

namespace Pinger {
    class Program {
        static void Main(string[] args) {
            Console.WriteLine("Starting Pinger!");
            IPingPong pinger = new PinPonger.Core.PinPonger(true);
            pinger.Init();
            pinger.Start();
        }
    }
}
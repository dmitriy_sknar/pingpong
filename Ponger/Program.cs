﻿using System;
using PinPonger.Core;

namespace Ponger {
    class Program {
        static void Main(string[] args) {
            Console.WriteLine("Starting Ponger!");
            IPingPong ponger = new PinPonger.Core.PinPonger(false);
            ponger.Init();
            ponger.Start();
        }
    }
}